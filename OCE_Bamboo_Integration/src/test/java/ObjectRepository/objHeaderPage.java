package ObjectRepository;

import org.openqa.selenium.By;

public class objHeaderPage
{
	public static By loginUserName = By.id("username");
	public static By loginPassword = By.id("password");
	public static By loginSubmit = By.id("Login");
	public static By passwordResetCancel = By.id("cancel-button");
	
	public static By doneButton = By.xpath("//button[@data-action='done']");
}
